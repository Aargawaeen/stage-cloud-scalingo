package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

// SearchServer is a HTTP interface for search information.
type SearchServer struct {
}

// Owner is the repository owner
type Owner struct {
	Login string
}

// Item is the single repository data structure
type Item struct {
	ID              int
	Name            string
	FullName        string `json:"full_name"`
	Owner           Owner
	Description     string
	CreatedAt       string `json:"created_at"`
	StargazersCount int    `json:"stargazers_count"`
	Language        string
}

// JSONData contains the GitHub API response
type JSONData struct {
	Count int `json:"total_count"`
	Items []Item
}

func printData(w http.ResponseWriter, data JSONData, langage string) {
	//var mu sync.Mutex
	const format = "%v\t%v\t%v\t%v\t%v\t\n"
	fmt.Fprintf(w, format, "Repository", "Stars", "Created at", "Description", "Language")
	fmt.Fprintf(w, format, "----------", "-----", "----------", "----------", "----------")
	for _, i := range data.Items {
		//go func(i Item, mu *sync.Mutex) {
		lang := i.Language
		if lang == langage {
			desc := i.Description
			if len(desc) > 50 {
				desc = string(desc[:50]) + "..."
			}
			t, err := time.Parse(time.RFC3339, i.CreatedAt)
			if err != nil {
				log.Fatal(err)
			}
			//mu.Lock()
			fmt.Fprintf(w, format, i.FullName, i.StargazersCount, t.Year(), desc, lang)
			//mu.Unlock()
			//}(i, &mu)

		}
	}
}

func MyHandler(w http.ResponseWriter, r *http.Request, lang string) {
	api := "https://api.github.com/search/repositories?q=updated&sort=updated&order=desc&per_page=100"
	res, err := http.Get(api)
	if err != nil {
		log.Fatal(err)
	}
	body, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		log.Fatal(err)
	}
	if res.StatusCode != http.StatusOK {
		log.Fatal("Unexpected status code", res.StatusCode)
	}
	data := JSONData{}
	err = json.Unmarshal(body, &data)
	if err != nil {
		log.Fatal(err)
	}
	printData(w, data, lang)
}

func FormHandler(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		fmt.Fprintf(w, "ParseForm() err: %v", err)
		return
	}
	lang := r.FormValue("lang")
	MyHandler(w, r, lang)
}
