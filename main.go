package main

import (
	"log"
	"net/http"
)

// InMemoryPlayerStore collects data about players in memory.

func main() {
	fileServer := http.FileServer(http.Dir("./"))
	http.Handle("/", fileServer)
	http.HandleFunc("/GET/search", FormHandler)
	log.Fatal(http.ListenAndServe(":5000", nil))
}
