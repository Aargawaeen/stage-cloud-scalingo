# Stage Cloud Scalingo



## Description

Vous pouvez retrouver ce module avec la commande "go get gitlab.com/Aargawaeen/stage-cloud-scalingo"
Ce projet récupère les 100 derniers repositories de github (grâce à leur API) et vous donne quelques informations sur ces derniers, vous pouvez également filtrer pour n'afficher que ceux codé pour un certain langage.
Après avoir build et lancé le binaire un serveur web démarre sur le port 5000
Rendez-vous à http://localhost:5000/
Ici rentrez le langage à filtrer et appuyez sur submit.
Vous êtes alors redirigé à http://localhost:5000/GET/search avec votre résultat.
(Remarque si vous allez directement à cette dernière adresse le filtre sera vide et vous filtrerez donc les projets où il n'y a pas de langage spécifié).
